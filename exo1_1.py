def div_eucl():
    diviseur, dividende = map(int, input("Entrez le diviseur et le dividende séparés par un espace : ").split())
    quotient = diviseur // dividende
    reste = diviseur % dividende
    return quotient, reste


def pgcd():
    a, b = map(int, input("Entrez les deux nombres désirés séparés par un espace : ").split())
    if b > a:
        a, b = b, a
    i = a % b
    if i == 0:
        return b
    else:
        while i != 0:
            i = a % b
            if i == 0:
                break
            a = b
            b = i
    return b


def inv_bezoult():
    nombre, zp = map(int, input("Entrez le nombre et l'espace désiré : ").split())
    ui, uj, vi, vj = 1, 0, 0, 1
    base = zp
    if zp > nombre:
        zp, nombre = nombre, zp
    q = nombre // zp
    r = nombre % zp
    print(str(nombre) + "=" + str(zp) + "*" + str(q) + "+" + str(r))
    i = 1
    while r != 0:
        ui, uj = ui*q+uj, ui
        vi, vj = vi*q+vj, vi
        nombre = zp
        zp = r
        q = nombre // zp
        r = nombre % zp
        i = i + 1
        print(str(nombre) + "=" + str(zp) + "*" + str(q) + "+" + str(r))

    i = i+1
    print(str(i))

    if ui*(-1)**i < 0:
        ui = ui*(-1)**i + base

    return ui


def succ_power_2():
    nombre = input("Entrez le nombre à décomposer : ")
    binary = "{0:b}".format(int(nombre))
    print(binary)
    nbr = 0
    txt = ""
    for i in range(len(binary)):
        nbr = (2**i)*int(binary[(len(binary)-(i+1))])+nbr
        if int(binary[(len(binary)-(i+1))]) != 0:
            txt += "2^"+str(i)+"+"

    print(nombre + " = " + txt[:-1])


